/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2016 Jean-Pierre Charras, jp.charras at wanadoo.fr
 * Copyright (C) 2011-2016 Wayne Stambaugh <stambaughw@verizon.net>
 * Copyright (C) 1992-2016 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/**
 * @file cvframe.cpp
 */

#include <fctsys.h>
#include <build_version.h>
#include <kiway_express.h>
#include <pgm_base.h>
#include <kiface_i.h>
#include <macros.h>
#include <confirm.h>
#include <eda_doc.h>
#include <eda_dde.h>
#include <gestfich.h>
#include <html_messagebox.h>
#include <wildcards_and_files_ext.h>
#include <fp_lib_table.h>
#include <netlist_reader.h>

#include "pkpcb_mainframe.h"
#include "pkpcb.h"
#include "listview_classes.h"
#include <invoke_pcb_dialog.h>
#include "pkpcb_id.h"
#include "dialog_edit_mnf.h"



#define FRAME_MIN_SIZE_X 450
#define FRAME_MIN_SIZE_Y 300


///@{
/// \ingroup config

/// Nonzero iff cvpcb should be kept open after saving files
static const wxString KeepPkpcbOpenEntry = "KeepPkpcbOpen";
///@}

BEGIN_EVENT_TABLE( PKPCB_MAINFRAME, KIWAY_PLAYER )

    // Menu events
    EVT_MENU( wxID_SAVE, PKPCB_MAINFRAME::OnSave )
    EVT_MENU( wxID_EXIT, PKPCB_MAINFRAME::OnQuit )
    EVT_MENU( wxID_HELP, PKPCB_MAINFRAME::GetKicadHelp )
    EVT_MENU( wxID_ABOUT, PKPCB_MAINFRAME::GetKicadAbout )
    EVT_MENU( ID_PKPCB_SET_MNF_INFO,       PKPCB_MAINFRAME::OnEditMnfInfo )
    EVT_MENU( ID_PKPCB_COPY_VALUE_TO_PART, PKPCB_MAINFRAME::OnCopyValueToPart )
    /*
    EVT_MENU( ID_CVPCB_CONFIG_KEEP_OPEN_ON_SAVE, PKPCB_MAINFRAME::OnKeepOpenOnSave )
    EVT_MENU( ID_CVPCB_EQUFILES_LIST_EDIT, PKPCB_MAINFRAME::OnEditEquFilesList )*/

    // Toolbar events
    /*EVT_TOOL( ID_CVPCB_QUIT, PKPCB_MAINFRAME::OnQuit )

    EVT_TOOL( ID_CVPCB_LIB_TABLE_EDIT, PKPCB_MAINFRAME::OnEditFootprintLibraryTable )
    EVT_TOOL( ID_CVPCB_CREATE_SCREENCMP, PKPCB_MAINFRAME::DisplayModule )
    EVT_TOOL( ID_CVPCB_GOTO_FIRSTNA, PKPCB_MAINFRAME::ToFirstNA )
    EVT_TOOL( ID_CVPCB_GOTO_PREVIOUSNA, PKPCB_MAINFRAME::ToPreviousNA )
    EVT_TOOL( ID_CVPCB_DEL_ASSOCIATIONS, PKPCB_MAINFRAME::DelAssociations )
    EVT_TOOL( ID_CVPCB_AUTO_ASSOCIE, PKPCB_MAINFRAME::AutomaticFootprintMatching )
    EVT_TOOL( ID_PCB_DISPLAY_FOOTPRINT_DOC, PKPCB_MAINFRAME::DisplayDocFile )
    EVT_TOOL( ID_CVPCB_FOOTPRINT_DISPLAY_FILTERED_LIST,
              PKPCB_MAINFRAME::OnSelectFilteringFootprint )
    EVT_TOOL( ID_CVPCB_FOOTPRINT_DISPLAY_PIN_FILTERED_LIST,
              PKPCB_MAINFRAME::OnSelectFilteringFootprint )
    EVT_TOOL( ID_CVPCB_FOOTPRINT_DISPLAY_BY_LIBRARY_LIST,
              PKPCB_MAINFRAME::OnSelectFilteringFootprint )
    EVT_TOOL( ID_CVPCB_FOOTPRINT_DISPLAY_BY_NAME,
              PKPCB_MAINFRAME::OnSelectFilteringFootprint )
    EVT_TEXT( ID_CVPCB_FILTER_TEXT_EDIT, PKPCB_MAINFRAME::OnEnterFilteringText )*/

    // Frame events
    EVT_CLOSE( PKPCB_MAINFRAME::OnCloseWindow )
    EVT_SIZE( PKPCB_MAINFRAME::OnSize )

    // UI event handlers
    /*EVT_UPDATE_UI( ID_CVPCB_CONFIG_KEEP_OPEN_ON_SAVE, PKPCB_MAINFRAME::OnUpdateKeepOpenOnSave )
    EVT_UPDATE_UI( ID_CVPCB_FOOTPRINT_DISPLAY_FILTERED_LIST, PKPCB_MAINFRAME::OnFilterFPbyKeywords)
    EVT_UPDATE_UI( ID_CVPCB_FOOTPRINT_DISPLAY_PIN_FILTERED_LIST, PKPCB_MAINFRAME::OnFilterFPbyPinCount )
    EVT_UPDATE_UI( ID_CVPCB_FOOTPRINT_DISPLAY_BY_LIBRARY_LIST, PKPCB_MAINFRAME::OnFilterFPbyLibrary )
    EVT_UPDATE_UI( ID_CVPCB_FOOTPRINT_DISPLAY_BY_NAME, PKPCB_MAINFRAME::OnFilterFPbyKeyName )*/

END_EVENT_TABLE()


#define CVPCB_MAINFRAME_NAME wxT( "PkpcbFrame" )


PKPCB_MAINFRAME::PKPCB_MAINFRAME( KIWAY* aKiway, wxWindow* aParent ) :
    KIWAY_PLAYER( aKiway, aParent, FRAME_CVPCB, wxT( "PkPCB" ), wxDefaultPosition,
        wxDefaultSize, KICAD_DEFAULT_DRAWFRAME_STYLE, CVPCB_MAINFRAME_NAME )
{
 
	m_compListBox           = NULL;
    m_mainToolBar           = NULL; 
    
    // Give an icon
    wxIcon icon;
    icon.CopyFromBitmap( KiBitmap( icon_pkpcb_xpm ) );
    SetIcon( icon );

    SetAutoLayout( true );

    LoadSettings( config() );

    if( m_FrameSize.x < FRAME_MIN_SIZE_X )
        m_FrameSize.x = FRAME_MIN_SIZE_X;

    if( m_FrameSize.y < FRAME_MIN_SIZE_Y )
        m_FrameSize.y = FRAME_MIN_SIZE_Y;

    // Set minimal frame width and height
    SetSizeHints( FRAME_MIN_SIZE_X, FRAME_MIN_SIZE_Y, -1, -1, -1, -1 );

    // Frame size and position
    SetSize( m_FramePos.x, m_FramePos.y, m_FrameSize.x, m_FrameSize.y );

    // create the status bar
    static const int dims[3] = { -1, -1, 250 };

    CreateStatusBar( 3 );
    SetStatusWidths( 3, dims );

    ReCreateMenuBar();
    ReCreateHToolbar();

    // Create list of available modules and components of the schematic
    
    BuildCmpListBox();

    m_auimgr.SetManagedWindow( this );

    UpdateTitle();

    EDA_PANEINFO horiz;
    horiz.HorizontalToolbarPane();

    EDA_PANEINFO info;
    info.InfoToolbarPane();


    if( m_mainToolBar )
        m_auimgr.AddPane( m_mainToolBar,
                          wxAuiPaneInfo( horiz ).Name( wxT( "m_mainToolBar" ) ).Top() );
	 if( m_compListBox )
        m_auimgr.AddPane( m_compListBox,
                          wxAuiPaneInfo( horiz ).Name( wxT( "m_compListBox" ) ).CentrePane() );

    m_auimgr.Update();
}


PKPCB_MAINFRAME::~PKPCB_MAINFRAME()
{
    m_auimgr.UnInit();
}


void PKPCB_MAINFRAME::BuildCmpListBox()
{
    wxString    msg;
    wxFont      guiFont = wxSystemSettings::GetFont( wxSYS_DEFAULT_GUI_FONT );

    if( m_compListBox == NULL )
    {
        m_compListBox = new COMPONENTS_LISTBOX( this, ID_PKPCB_COMPONENT_LIST,
                                                wxDefaultPosition, wxDefaultSize );
        m_compListBox->SetFont( wxFont( guiFont.GetPointSize(),
                                        wxFONTFAMILY_MODERN,
                                        wxFONTSTYLE_NORMAL,
                                        wxFONTWEIGHT_NORMAL ) );
    }
/* TODOL to delete?
    m_compListBox->m_ComponentList.Clear();

    for( unsigned i = 0;  i < m_netlist.GetCount();  i++ )
    {
        component = m_netlist.GetComponent( i );

        msg.Printf( CMP_FORMAT, m_compListBox->GetCount() + 1,
                    GetChars( component->GetReference() ),
                    GetChars( component->GetValue() ),
                    "",
                    "");
        m_compListBox->m_ComponentList.Add( msg );
    }

    if( m_compListBox->m_ComponentList.Count() )
    {
        m_compListBox->SetItemCount( m_compListBox->m_ComponentList.Count() );
        m_compListBox->SetSelection( 0, true );
        m_compListBox->RefreshItems( 0L, m_compListBox->m_ComponentList.Count()-1 );
        m_compListBox->UpdateWidth();
    }
*/

	LoadComponents();
}


void PKPCB_MAINFRAME::LoadSettings( wxConfigBase* aCfg )
{
    EDA_BASE_FRAME::LoadSettings( aCfg );
    
    aCfg->Read( KeepPkpcbOpenEntry, &m_keepPkpcbOpen, true );
}

void PKPCB_MAINFRAME::FormatComponent(wxString& msg, COMPONENT* component)
{
	msg.Printf( CMP_FORMAT, m_compListBox->GetCount() + 1,
                    GetChars( component->GetReference() ),
                    GetChars( component->GetValue() ),
                    GetChars( component->GetMnfName() ),
                    GetChars( component->GetMnfPart() )
              );
}

void PKPCB_MAINFRAME::LoadComponents()
{
	m_compListBox->m_ComponentList.Clear();
	
	wxString        msg;
	// Populates the component list box:
    for( unsigned i = 0;  i < m_netlist.GetCount();  i++ )
    {
        FormatComponent(msg, m_netlist.GetComponent( i ));
        m_compListBox->AppendLine( msg );
    }

    if( !m_netlist.IsEmpty() )
        m_compListBox->SetSelection( 0, true );
}


void PKPCB_MAINFRAME::SaveSettings( wxConfigBase* aCfg )
{
    EDA_BASE_FRAME::SaveSettings( aCfg );

    aCfg->Write( KeepPkpcbOpenEntry, m_keepPkpcbOpen );
}


void PKPCB_MAINFRAME::OnSize( wxSizeEvent& event )
{
    event.Skip();
}


void PKPCB_MAINFRAME::OnQuit( wxCommandEvent& event )
{
    Close( false );
}


void PKPCB_MAINFRAME::OnCloseWindow( wxCloseEvent& Event )
{
    if( m_modified )
    {
        wxString msg = _( "Component part links modified.\nSave before exit ?" );
        int ii = DisplayExitDialog( this, msg );

        switch( ii )
        {
        case wxID_CANCEL:
            Event.Veto();
            return;

        case wxID_NO:
            break;

        case wxID_YES:
            SaveMnfPartAssociation();
            break;
        }
    }

    m_modified = false;

    Destroy();
    return;
}


void PKPCB_MAINFRAME::OnSelectComponent( wxListEvent& event )
{
//    COMPONENT* component = GetSelectedComponent();
}

void PKPCB_MAINFRAME::ChangeFocus( bool aMoveRight )
{
    //wxWindow* hasFocus = wxWindow::FindFocus();

    if( aMoveRight )
    {
        //if( hasFocus == m_libListBox )
            m_compListBox->SetFocus();
        //else if( hasFocus == m_compListBox )
        //    m_footprintListBox->SetFocus();
        //else if( hasFocus == m_footprintListBox )
        //    m_libListBox->SetFocus();
    }
    else
    {
        //if( hasFocus == m_libListBox )
        //    m_footprintListBox->SetFocus();
        //else if( hasFocus == m_compListBox )
        //      m_libListBox->SetFocus();
        //else if( hasFocus == m_footprintListBox )
            m_compListBox->SetFocus();
    }
}




bool PKPCB_MAINFRAME::OpenProjectFiles( const std::vector<wxString>& aFileSet, int aCtl )
{
    return true;
}






void PKPCB_MAINFRAME::DisplayStatus()
{
    wxString   msg = "???";
    SetStatusText( msg, 1 );
}



void PKPCB_MAINFRAME::UpdateTitle()
{
    wxString    title = "pkpcb";

    SetTitle( title );
}


void PKPCB_MAINFRAME::SendMessageToEESCHEMA()
{

    std::string packet = StrPrintf( "$PART: \"%s\"", "" );

    if( Kiface().IsSingle() )
        SendCommand( MSG_TO_SCH, packet.c_str() );
    else
        Kiway().ExpressMail( FRAME_SCH, MAIL_CROSS_PROBE, packet, this );
}


int PKPCB_MAINFRAME::ReadSchematicNetlist( const std::string& aNetlist )
{
    STRING_LINE_READER*     strrdr = new STRING_LINE_READER( aNetlist, "Eeschema via Kiway" );
    KICAD_NETLIST_READER    netrdr( strrdr, &m_netlist );

    m_netlist.Clear();

    try
    {
        netrdr.LoadNetlist();
    }
    catch( const IO_ERROR& ioe )
    {
        wxString msg = wxString::Format( _( "Error loading netlist.\n%s" ), ioe.What().GetData() );
        wxMessageBox( msg, _( "Netlist Load Error" ), wxOK | wxICON_ERROR );
        return 1;
    }

    // We also remove footprint name if it is "$noname" because this is a dummy name,
    // not the actual name of the footprint.
    for( unsigned ii = 0; ii < m_netlist.GetCount(); ii++ )
    {
        if( m_netlist.GetComponent( ii )->GetFPID().GetFootprintName() == std::string( "$noname" ) )
            m_netlist.GetComponent( ii )->SetFPID( FPID( wxEmptyString ) );
    }

    // Sort components by reference:
    m_netlist.SortByReference();


	LoadComponents();
	
    return 0;
}





void PKPCB_MAINFRAME::KiwayMailIn( KIWAY_EXPRESS& mail )
{
    const std::string& payload = mail.GetPayload();

    DBG(printf( "%s: %s\n", __func__, payload.c_str() );)

	switch( mail.Command() )
    {
    case MAIL_EESCHEMA_NETLIST:
        ReadSchematicNetlist( payload );
        /* @todo
        Go into SCH_EDIT_FRAME::OnOpenPkpcb( wxCommandEvent& event ) and trim GNL_ALL down.
        */
        break;

    default:
        ;       // ignore most
    }
}

void PKPCB_MAINFRAME::SaveMnfPartAssociation()
{
    STRING_FORMATTER sf;

    m_netlist.FormatBackAnnotationMnf( &sf );

    Kiway().ExpressMail( FRAME_SCH, MAIL_BACKANNOTATE_MNFPART, sf.GetString() );

    SetStatusText( _("Footprint association sent to Eeschema") );
}

void PKPCB_MAINFRAME::OnCopyValueToPart( wxCommandEvent& aEvent )
{
	std::vector<COMPONENT*> components;
    
    for ( int i = m_compListBox->GetFirstSelected(); i != -1; i = m_compListBox->GetNextSelected(i) )
		components.push_back(m_netlist.GetComponent( i ));
		
	if(components.empty() )
        return;
        
    for (int componentIndex = m_compListBox->GetFirstSelected(); componentIndex != -1;  componentIndex = m_compListBox->GetNextSelected(componentIndex))
	{
		COMPONENT* component = m_netlist.GetComponent( componentIndex );
		
		if (component->GetValue() != component->GetMnfPart())
		{
			component->SetMnfPart(component->GetValue());
			m_modified = true;
					
			 // create the new component description
			wxString   description;
			
			FormatComponent(description, component);
			m_compListBox->SetString( componentIndex, description );
			   
		}
	}
	
	m_auimgr.Update();
	// update the statusbar
	DisplayStatus();
}

void PKPCB_MAINFRAME::OnEditMnfInfo( wxCommandEvent& aEvent )
{
    std::vector<COMPONENT*> components;
    
    for ( int i = m_compListBox->GetFirstSelected(); i != -1; i = m_compListBox->GetNextSelected(i) )
		components.push_back(m_netlist.GetComponent( i ));
		
	if(components.empty() )
        return;


	bool mnfNameMultiple = false;
	bool mnfPartMultiple = false;
	
	wxString mnfNameValue = "";
	wxString mnfPartValue = "";
	
	
	for (COMPONENT* component: components)
	{
		if (!component->GetMnfName().IsEmpty())
		{
			if (mnfNameValue.empty())
				mnfNameValue = component->GetMnfName();
			else
			{
				if (mnfNameValue != component->GetMnfName())
				{
					mnfNameMultiple = true;
					mnfNameValue = "";
				}
			}
		}

		if (!component->GetMnfPart().IsEmpty())
		{
			if (mnfPartValue.empty())
				mnfPartValue = component->GetMnfPart();
			else
			{
				if (mnfPartValue != component->GetMnfPart())
				{
					mnfPartMultiple = true;
					mnfPartValue = "";
				}
			}
		}
	}
	
	
    DIALOG_EDIT_MNF dialog( this, components.size(), mnfNameMultiple, mnfNameValue, mnfPartMultiple, mnfPartValue);

    if ( dialog.ShowModal() == wxID_OK)
		UpdateMnfInfo(mnfNameMultiple, mnfNameValue, mnfPartMultiple, mnfPartValue);
    
}



void PKPCB_MAINFRAME::UpdateMnfInfo( bool& mnfNameMultiple, wxString& mnfNameValue, bool& mnfPartMultiple, wxString& mnfPartValue)
{
	
    for (int componentIndex = m_compListBox->GetFirstSelected(); componentIndex != -1;  componentIndex = m_compListBox->GetNextSelected(componentIndex))
    {
        COMPONENT* component = m_netlist.GetComponent( componentIndex );
		
		if (!mnfNameMultiple)
		{
			component->SetMnfName(mnfNameValue);
			m_modified = true;
		}
		
		if (!mnfPartMultiple)
		{
			component->SetMnfPart(mnfPartValue);
			m_modified = true;
		}
			
		 // create the new component description
        wxString   description;
        
        FormatComponent(description, component);
        m_compListBox->SetString( componentIndex, description );
       
	}
	
	 m_auimgr.Update();
	// update the statusbar
    DisplayStatus();
    
    
}

void PKPCB_MAINFRAME::OnSave( wxCommandEvent& event )
{
	SaveMnfPartAssociation();
}

