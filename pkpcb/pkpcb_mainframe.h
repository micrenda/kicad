/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2016 Jean-Pierre Charras, jp.charras at wanadoo.fr
 * Copyright (C) 1992-2016 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/**
 * @file cvpcb_mainframe.h
 */

#ifndef _PKPCB_MAINFRAME_H_
#define _PKPCB_MAINFRAME_H_

#include <wx/listctrl.h>
#include <wx/filename.h>
#include <pcb_netlist.h>
#include <footprint_info.h>

#include <wxBasePcbFrame.h>
#include <config_params.h>


/*  Forward declarations of all top-level window classes. */
class wxAuiToolBar;
class COMPONENTS_LISTBOX;
class DISPLAY_FOOTPRINTS_FRAME;
class COMPONENT;
class FP_LIB_TABLE;

namespace PK { struct IFACE; }

/**
 * The CvPcb application main window.
 */
class PKPCB_MAINFRAME : public KIWAY_PLAYER
{
    friend struct PK::IFACE;

    bool                      m_keepPkpcbOpen;
    NETLIST                   m_netlist;
    wxAuiToolBar*             m_mainToolBar;
    COMPONENTS_LISTBOX*       m_compListBox;
    

public:
    wxString                  m_DocModulesFileName;

protected:
    bool            m_modified;
    PARAM_CFG_ARRAY m_projectFileParams;

    PKPCB_MAINFRAME( KIWAY* aKiway, wxWindow* aParent );

public:
    ~PKPCB_MAINFRAME();

    bool OpenProjectFiles( const std::vector<wxString>& aFileSet, int aCtl=0 ) override;

    void KiwayMailIn( KIWAY_EXPRESS& aEvent ) override;

    /**
     * Function OnSelectComponent
     * Called when clicking on a component in component list window
     * * Updates the filtered footprint list, if the filtered list option is selected
     * * Updates the current selected footprint in footprint list
     * * Updates the footprint shown in footprint display window (if opened)
     */
    void             OnSelectComponent( wxListEvent& event );

    void             OnQuit( wxCommandEvent& event );
    void             OnSave( wxCommandEvent& event );
    void             OnCloseWindow( wxCloseEvent& Event );
    void             OnSize( wxSizeEvent& SizeEvent );
    void             ReCreateHToolbar();
    virtual void     ReCreateMenuBar() override;
    void 			 OnCopyValueToPart( wxCommandEvent& event );
    
    void             ChangeFocus( bool aMoveRight );

    /**
     * Function LoadProjectFile
     * reads the CvPcb configuration parameter from the project (.pro) file \a aFileName
     */
    void LoadProjectFile();

    /**
     * Function SaveProjectFile
     * Saves the CvPcb configuration parameter from the project (.pro) file \a aFileName
     */
    void SaveProjectFile();

    void LoadSettings( wxConfigBase* aCfg ) override;

    void SaveSettings( wxConfigBase* aCfg ) override;

	/**
	 * Function LoadComponents
	 * Refresh the components in listbox with the components in m_netlist
	 * 
	 */
	 void LoadComponents();
	
    /**
     * Function DisplayStatus
     * updates the information displayed on the status bar at bottom of the main frame.
     *
     * When the library or component list controls have the focus, the footprint assignment
     * status of the components is displayed in the first status bar pane and the list of
     * filters for the selected component is displayed in the second status bar pane.  When
     * the footprint list control has the focus, the description of the selected footprint is
     * displayed in the first status bar pane and the key words for the selected footprint are
     * displayed in the second status bar pane.  The third status bar pane always displays the
     * current footprint list filtering.
     */
    void             DisplayStatus();

	/**
	 * Function	BuildCmpListBox
	 * 
	 * Create the list box showing the components
	 * 
	 */ 
	void             BuildCmpListBox();

    /**
     * Function GetProjectFileParameters
     * return project file parameter list for CvPcb.
     * <p>
     * Populate the project file parameter array specific to CvPcb if it hasn't
     * already been populated and return a reference to the array to the caller.
     * Creating the parameter list at run time has the advantage of being able
     * to define local variables.  The old method of statically building the array
     * at compile time requiring global variable definitions.
     * </p>
     *
     * @return A reference to a PARAM_CFG_ARRAY contain the project settings for CvPcb.
     */
    PARAM_CFG_ARRAY& GetProjectFileParameters( void );


    /**
     * Function ReadNetList
     * reads the netlist (.net) file defined by #m_NetlistFileName.
     * and the corresponding cmp to footprint (.cmp) link file
     * @param aNetlist is the netlist from eeschema in kicad s-expr format.
     */
    int  ReadSchematicNetlist( const std::string& aNetlist );
    
    /**
     * Function SavePartAssociation
     * Save the associated manufacter part association to the schematics
     * @param aNetlist is the netlist from eeschema in kicad s-expr format.
     */
    void SaveMnfPartAssociation();
    
    /**
     * Function UpdateTitle
     * sets the main window title bar text.
     * <p>
     * If no current project open( eeschema run outside kicad manager with no schematic loaded),
     * the title is set to the application name appended with "no project".
     * Otherwise, the title shows the project name.
     */
    void UpdateTitle();

    /**
     * Function SendMessageToEESCHEMA
     * Send a remote command to Eeschema via a socket,
     * Commands are
     * $PART: "reference"   put cursor on component anchor
     */
    void SendMessageToEESCHEMA();

	/**
	 * Function OnEditMnfInfo
	 * Open dialog to edit manufacturer info. It allows to edit the manufacturer info
	 * for a single component or for a list of comoponents.
	 */
	void OnEditMnfInfo( wxCommandEvent& aEvent );
	
	
	/**
	 * Functuion FormatComponent
	 * Format a component to be displayed into list box.
	 */	
	void FormatComponent(wxString& msg, COMPONENT* component);
		
	/**
	 *  Function UpdateMnfInfo
	 *  Save the modifications and update the list box
	 */
	void UpdateMnfInfo( bool& mnfNameMultiple, wxString& mnfNameValue, bool& mnfPartMultiple, wxString& mnfPartValue);


private:
    // UI event handlers.
    // Keep consistent the display state of toggle menus or tools in toolbar


    DECLARE_EVENT_TABLE()
};

#endif  //#ifndef _PKPCB_MAINFRAME_H_
