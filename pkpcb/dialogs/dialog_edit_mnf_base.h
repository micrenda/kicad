///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Feb 16 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __DIALOG_EDIT_MNF_BASE_H__
#define __DIALOG_EDIT_MNF_BASE_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
class DIALOG_SHIM;

#include "dialog_shim.h"
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

#define ID_MNF_NAME 1000
#define ID_MNF_PART 1001

///////////////////////////////////////////////////////////////////////////////
/// Class DIALOG_EDIT_MNF_BASE
///////////////////////////////////////////////////////////////////////////////
class DIALOG_EDIT_MNF_BASE : public DIALOG_SHIM
{
	private:
	
	protected:
		wxStaticText* m_staticText1;
		wxTextCtrl* m_mnfNameText;
		wxStaticText* m_staticText2;
		wxTextCtrl* m_mnfPartText;
		wxStaticText* m_items_qty;
		wxStdDialogButtonSizer* m_sdbSizer1;
		wxButton* m_sdbSizer1OK;
		wxButton* m_sdbSizer1Cancel;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnMnfNameUnfocus( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnMnfNameFocus( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnMnfPartUnfocus( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnMnfPartFocus( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnCancelButton( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOKButton( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		DIALOG_EDIT_MNF_BASE( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Part manufacturer info"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 494,204 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER ); 
		~DIALOG_EDIT_MNF_BASE();
	
};

#endif //__DIALOG_EDIT_MNF_BASE_H__
