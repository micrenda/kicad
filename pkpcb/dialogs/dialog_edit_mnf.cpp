#include "dialog_edit_mnf.h"

static wxString MULTIPLE_VALUES = _( "(multiple values)" );

DIALOG_EDIT_MNF::DIALOG_EDIT_MNF(
	PKPCB_MAINFRAME* parent, 
	int componentsCount, 
	bool& isMnfNameMultiple, 
	wxString& mnfNameValue, 
	bool& isMnfPartMultiple, 
	wxString& mnfPartValue): 
	DIALOG_EDIT_MNF_BASE(parent),
	m_componentsCount(componentsCount),
	m_isMnfNameMultiple(isMnfNameMultiple), 
	m_mnfNameValue(mnfNameValue), 
	m_isMnfPartMultiple(isMnfPartMultiple), 
	m_mnfPartValue(mnfPartValue)
{
	SetTitle( _( "Edit manufacturer info" ) );
	
	InitDialog();
	
	FixOSXCancelButtonIssue();

    // Now all widgets have the size fixed, call FinishDialogSettings
    FinishDialogSettings();
}


void DIALOG_EDIT_MNF::InitDialog()
{
	
	switch(m_componentsCount)
	{
		case 0:
			m_items_qty->SetLabel(_( "(no compnent selected)" ));
			
		break;
		
		case 1:
			m_items_qty->SetLabel("");
		break;
		
		default:
			m_items_qty->SetLabel(_( "(%d components selected)" ));
		break;
	}	
		
	m_items_qty->Show( m_componentsCount != 1 );
	
	m_sdbSizer1OK->SetFocus();
	
	if (m_isMnfNameMultiple)
		m_mnfNameText->SetValue(MULTIPLE_VALUES);
	else
		m_mnfNameText->SetValue(m_mnfNameValue);
		
	if (m_isMnfPartMultiple)
		m_mnfPartText->SetValue(MULTIPLE_VALUES);
	else
		m_mnfPartText->SetValue(m_mnfPartValue);
		
	UpdateFieldsStyle();
	
}

void DIALOG_EDIT_MNF::UpdateFieldsStyle()
{
	if (m_isMnfNameMultiple && m_mnfNameText->GetValue() == MULTIPLE_VALUES)
		m_mnfNameText->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_GRAYTEXT ) );
	else
		m_mnfNameText->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );

	if (m_isMnfPartMultiple && m_mnfPartText->GetValue() == MULTIPLE_VALUES)
		m_mnfPartText->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_GRAYTEXT ) );
	else
		m_mnfPartText->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );
}



void DIALOG_EDIT_MNF::OnMnfNameFocus( wxFocusEvent& event )
{
	if (m_isMnfNameMultiple && m_mnfNameText->GetValue() == MULTIPLE_VALUES)
		m_mnfNameText->SetValue("");
		
	UpdateFieldsStyle();
}

void DIALOG_EDIT_MNF::OnMnfPartFocus( wxFocusEvent& event )
{
	if (m_isMnfPartMultiple && m_mnfPartText->GetValue() == MULTIPLE_VALUES)
		m_mnfPartText->SetValue("");
		
	UpdateFieldsStyle();
}

void DIALOG_EDIT_MNF::OnMnfNameUnfocus( wxFocusEvent& event )
{
	if (m_isMnfNameMultiple && m_mnfNameText->GetValue() == "")
		m_mnfNameText->SetValue(MULTIPLE_VALUES);
		
	UpdateFieldsStyle();
}

void DIALOG_EDIT_MNF::OnMnfPartUnfocus( wxFocusEvent& event )
{
	if (m_isMnfPartMultiple && m_mnfPartText->GetValue() == "")
		m_mnfPartText->SetValue(MULTIPLE_VALUES);
		
	UpdateFieldsStyle();
}


void DIALOG_EDIT_MNF::OnCancelButton( wxCommandEvent& event )
{
	EndModal( wxID_CANCEL );
}

void DIALOG_EDIT_MNF::OnOKButton( wxCommandEvent& event )
{
	if (!m_isMnfNameMultiple || m_mnfNameText->GetValue() != MULTIPLE_VALUES) 
	{
		m_isMnfNameMultiple = false;
		m_mnfNameValue = m_mnfNameText->GetValue();
	}
	else
	{
		m_isMnfNameMultiple = true;
		m_mnfNameValue = "";
	}
	
	if (!m_isMnfPartMultiple || m_mnfPartText->GetValue() != MULTIPLE_VALUES) 
	{
		m_isMnfPartMultiple = false;
		m_mnfPartValue = m_mnfPartText->GetValue();
	}
	else
	{
		m_isMnfPartMultiple = true;
		m_mnfPartValue = "";
	}
	
	EndModal( wxID_OK );
}



