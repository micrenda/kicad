#ifndef __dialog_edit_mnf__
#define __dialog_edit_mnf__


#include "../pkpcb_mainframe.h"
#include "dialog_edit_mnf_base.h"
#include <vector>
#include "netlist_reader.h"

//// end generated include

/** Implementing DIALOG_EDIT_MNF_BASE */
class DIALOG_EDIT_MNF : public DIALOG_EDIT_MNF_BASE
{
	public:
		/** Constructor */
		DIALOG_EDIT_MNF( PKPCB_MAINFRAME* parent, int componentsCount, bool& isMnfNameMultiple, wxString& mnfNameValue, bool& isMnfPartMultiple, wxString& mnfPartValue);
		
	protected:

		void InitDialog() override;
		
		void OnMnfNameFocus( wxFocusEvent& event ) override;
		void OnMnfPartFocus( wxFocusEvent& event ) override;
		void OnMnfNameUnfocus( wxFocusEvent& event ) override;
		void OnMnfPartUnfocus( wxFocusEvent& event ) override;
		
		
		void OnCancelButton( wxCommandEvent& event ) override;
		void OnOKButton( wxCommandEvent& event ) override;
		
		void UpdateFieldsStyle();

		int  m_componentsCount;
		
		bool& m_isMnfNameMultiple;
		wxString& m_mnfNameValue;
				
		bool& m_isMnfPartMultiple;
		wxString& m_mnfPartValue;
		
	
};

#endif // __dialog_edit_mnf__
