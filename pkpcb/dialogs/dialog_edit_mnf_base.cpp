///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Feb 16 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "dialog_edit_mnf_base.h"

///////////////////////////////////////////////////////////////////////////

DIALOG_EDIT_MNF_BASE::DIALOG_EDIT_MNF_BASE( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : DIALOG_SHIM( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText1 = new wxStaticText( this, wxID_ANY, wxT("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer1->Add( m_staticText1, 0, wxALL, 5 );
	
	m_mnfNameText = new wxTextCtrl( this, ID_MNF_NAME, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !m_mnfNameText->HasFlag( wxTE_MULTILINE ) )
	{
	m_mnfNameText->SetMaxLength( 255 );
	}
	#else
	m_mnfNameText->SetMaxLength( 255 );
	#endif
	m_mnfNameText->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	
	bSizer1->Add( m_mnfNameText, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Part Ref"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer1->Add( m_staticText2, 0, wxALL, 5 );
	
	m_mnfPartText = new wxTextCtrl( this, ID_MNF_PART, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !m_mnfPartText->HasFlag( wxTE_MULTILINE ) )
	{
	m_mnfPartText->SetMaxLength( 255 );
	}
	#else
	m_mnfPartText->SetMaxLength( 255 );
	#endif
	bSizer1->Add( m_mnfPartText, 0, wxALL|wxEXPAND, 5 );
	
	m_items_qty = new wxStaticText( this, wxID_ANY, wxT("(no compnent selected)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_items_qty->Wrap( -1 );
	m_items_qty->SetFont( wxFont( 10, 74, 93, 90, false, wxT("Sans") ) );
	m_items_qty->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_GRAYTEXT ) );
	
	bSizer1->Add( m_items_qty, 0, wxALL, 5 );
	
	
	bSizer1->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_sdbSizer1 = new wxStdDialogButtonSizer();
	m_sdbSizer1OK = new wxButton( this, wxID_OK );
	m_sdbSizer1->AddButton( m_sdbSizer1OK );
	m_sdbSizer1Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer1->AddButton( m_sdbSizer1Cancel );
	m_sdbSizer1->Realize();
	
	bSizer1->Add( m_sdbSizer1, 0, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer1 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_mnfNameText->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfNameUnfocus ), NULL, this );
	m_mnfNameText->Connect( wxEVT_SET_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfNameFocus ), NULL, this );
	m_mnfPartText->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfPartUnfocus ), NULL, this );
	m_mnfPartText->Connect( wxEVT_SET_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfPartFocus ), NULL, this );
	m_sdbSizer1Cancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DIALOG_EDIT_MNF_BASE::OnCancelButton ), NULL, this );
	m_sdbSizer1OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DIALOG_EDIT_MNF_BASE::OnOKButton ), NULL, this );
}

DIALOG_EDIT_MNF_BASE::~DIALOG_EDIT_MNF_BASE()
{
	// Disconnect Events
	m_mnfNameText->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfNameUnfocus ), NULL, this );
	m_mnfNameText->Disconnect( wxEVT_SET_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfNameFocus ), NULL, this );
	m_mnfPartText->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfPartUnfocus ), NULL, this );
	m_mnfPartText->Disconnect( wxEVT_SET_FOCUS, wxFocusEventHandler( DIALOG_EDIT_MNF_BASE::OnMnfPartFocus ), NULL, this );
	m_sdbSizer1Cancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DIALOG_EDIT_MNF_BASE::OnCancelButton ), NULL, this );
	m_sdbSizer1OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DIALOG_EDIT_MNF_BASE::OnOKButton ), NULL, this );
	
}
